#include <iostream>

using std::cout;
using std::endl;

template<typename Iterator, typename Function>
void foreach(Iterator first, Iterator last, Function fn) {
    for (; first != last; ++first) fn(*first);
}

template<typename Iterator, typename Function>
void generate(Iterator first, Iterator last, Function fn) {
    for (; first != last; ++first) *first = fn();
}

template<typename Iterator, typename Function>
void map(Iterator first, Iterator last, Function fn) {
    for (; first != last; ++first) *first = fn(*first);
}

template<typename RetType = long, typename Iterator, typename Function>
RetType reduce(Iterator first, Iterator last, Function fn) {
    RetType result = *first;
    for (++first; first != last; ++first) result = fn(result, *first);
    return result;
}

template<typename RetType, typename Iterator, typename MapFn, typename ReduceFn>
RetType map_reduce(Iterator first, Iterator last, MapFn mapFn, ReduceFn reduceFn) {
    RetType result = mapFn(*first);
    for (++first; first != last; ++first) result = reduceFn(result, mapFn(*first));
    return result;
}

int main() {
    auto const N = 10U;
    int        numbers[N]{};
    foreach(numbers, numbers + N, [](auto n) { cout << n << " "; });
    cout << endl;

    generate(numbers, numbers + N, [next = 0]() mutable { return ++next, next; });
    foreach(numbers, numbers + N, [](auto n) { cout << n << " "; });
    cout << endl;

    auto mapper  = [](auto x) { return x * x; };
    auto reducer = [](auto acc, auto x) { return acc + x; };
    auto result  = map_reduce<long>(numbers, numbers + N, mapper, reducer);
    cout << result << endl;
}
