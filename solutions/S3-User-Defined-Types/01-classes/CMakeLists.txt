cmake_minimum_required(VERSION 3.18)
project(01_classes)

set(CMAKE_CXX_STANDARD 17)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(person-app person-app.cxx)
target_compile_options(person-app PRIVATE ${WARN})


