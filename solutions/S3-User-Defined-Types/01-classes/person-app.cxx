#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using std::cout;
using std::string;
using namespace std::literals;

class Person {
    string   name;
    unsigned age;
public:
    Person(string const& name_, unsigned age_)
            : name{name_}, age{age_} {
        cout << "Person{" << name << ", " << age << "} @ " << this << "\n";
    }

    ~Person() {
        cout << "~Person() @ " << this << "\n";
    }

    string to_string() const {
        auto out = std::ostringstream{};
        out << "Person{" << name << ", " << age << "} @ " << this;
        return out.str();
    }

    auto getName() const -> string const& { return name; }

    auto getAge() const { return age; }

    void setName(string name_) {
        name = std::move(name_);
    }
};

int main() {
    cout << "[main] enter\n";
    {
        auto p = Person{"Nisse Hult"s, 42};
        cout << "p: " << p.to_string() << "\n";

        p.setName("Per Silja"s);
        cout << "p: " << p.to_string() << "\n";
    }
    {
        auto persons = std::vector<Person>{
                Person{"Justin Time"s, 32}
        };

        auto p = Person{"Nisse Hult"s, 42};
        persons.push_back(p);

        persons.push_back(Person{"Per Silja"s, 52});

        persons.emplace_back("Anna Conda"s, 62);

        for (auto const& person: persons) {
             cout << person.to_string() << "\n";
        }
    }
    cout << "[main] exit\n";
}
