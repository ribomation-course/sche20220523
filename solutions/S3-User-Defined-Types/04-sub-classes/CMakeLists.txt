cmake_minimum_required(VERSION 3.18)
project(04_sub_classes)

set(CMAKE_CXX_STANDARD 17)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(person-types 
        person.hxx
        student.hxx
        employee.hxx
        manager.hxx
        app.cxx
        )
target_compile_options(person-types PRIVATE ${WARN})
