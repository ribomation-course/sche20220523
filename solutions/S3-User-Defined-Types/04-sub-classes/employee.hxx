#pragma once
#include <string>
#include "person.hxx"

namespace ribomation::persons {
    using namespace std::string_literals;
    using std::string;

    struct Employee : Person {
        Employee(string name_, unsigned id) 
            : Person{std::move(name_)}, employee_id{id} {}

        auto to_string() const -> string override {
            return "Employee{id="s + std::to_string(employee_id) + " "s + Person::to_string() + "}"s;
        }

    private:
        unsigned employee_id;
    };

}


