#pragma once

#include <string>

namespace ribomation::persons {
    using std::string;
    using namespace std::string_literals;

    struct Person {
        virtual ~Person() = default;
        virtual auto to_string() const -> string {
            return "Person{"s + name + "}"s;
        }
        
    protected:
        Person(string name_) : name{std::move(name_)} {}

    private:
        const string name;
    };

}


