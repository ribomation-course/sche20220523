#include <iostream>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

using std::cout;
using std::string;
using namespace std::literals;

class Person {
    string   name = "No Name"s;
    unsigned age  = 0;
public:
    Person() {
        cout << "Person() @ " << this << "\n";
    }

    Person(string name_, unsigned age_) : name{std::move(name_)}, age{age_} {
        cout << "Person{" << name << ", " << age << "} @ " << this << "\n";
    }

    ~Person() {
        cout << "~Person() @ " << this << "\n";
    }

    string to_string() const {
        auto out = std::ostringstream{};
        out << "Person{" << name << ", " << age << "} @ " << this;
        return out.str();
    }

    void setName(string name_) { name = std::move(name_); }
    void setAge(unsigned age_) { age = age_; }
};

void usecase_1() {
    cout << "-- single int --\n";
    auto ptr = new int{42};
    cout << *ptr << " @ " << ptr << "\n";
    delete ptr;
}

void usecase_2() {
    cout << "-- array of ints --\n";
    auto const N   = 25;
    auto       ptr = new int[N];
    cout << *ptr << " @ " << ptr << "\n";

    for (auto k = 0; k < N; ++k) ptr[k] = k + 1;

    for (auto k = 0; k < N; ++k) cout << ptr[k] << " ";
    cout << "\n";

    delete[] ptr;
}

void usecase_3() {
    cout << "-- single person --\n";
    auto ptr = new Person{"Per Silja"s, 42};
    cout << ptr->to_string() << "\n";
    delete ptr;
}

void usecase_4() {
    cout << "-- array of person objects --\n";
    auto const N   = 5;
    auto       ptr = new Person[N];
    cout << (*ptr).to_string() << " @ " << ptr << "\n";

    for (auto k = 0; k < N; ++k) {
        ptr[k].setName("nisse-"s + std::to_string(k + 1));
        ptr[k].setAge(10 * k + 18);
    }

    for (auto k = 0; k < N; ++k) cout << ptr[k].to_string() << "\n";

    delete[] ptr;
}

int main() {
    usecase_1();
    usecase_2();
    usecase_3();
    usecase_4();
}
