#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <cstring>

namespace ribomation::domain {
    using std::cout;

    inline auto strClone(char const* str) -> char* {  //strdup() is using malloc()
        if (str == nullptr) return nullptr;
        return ::strcpy(new char[::strlen(str) + 1], str);
    }

    class Person {
        char* name = nullptr;
        unsigned age = 0;

    public:
        ~Person() {
            cout << "~Person @ " << this << "\n";
            delete[] name;
        }

        Person() {
            cout << "Person{} @ " << this << "\n";
            name = strClone("");
        }

        Person(const char* n, unsigned a) : name{strClone(n)}, age{a} {
            cout << "Person{const char* " << n << ", " << a << "} @ " << this << "\n";
        }

        Person( Person const& that) : name{strClone(that.name)}, age{that.age} {
            cout << "Person{const Person& " << &that << "} @ " << this << "\n";
        }

        Person& operator=(Person const& rhs) noexcept {
            if (this != &rhs) {
                delete[] name;
                name = strClone(rhs.name);
                age = rhs.age;
            }
            cout << "operator={Person const& " << &rhs << "} @ " << this << "\n";
            return *this;
        }

        auto incrAge() {
            return ++age;
        }

        [[nodiscard]] auto toString() const -> std::string {
            auto buf = std::ostringstream{};
            buf << "Person(" << (name ? name : "??") << ", " << age << ") @ " << this;
            return buf.str();
        }
    };

}

namespace rm = ribomation::domain;

auto func(rm::Person q) -> rm::Person {
    std::cout << "[func] q: " << q.toString() << "\n";
    q.incrAge();
    std::cout << "[func] q: " << q.toString() << "\n";
    return q;
}

auto g = rm::Person{"Anna Conda", 42};

int main() {
    using std::cout;
    
    cout << "[main] enter\n";
    cout << "[main] g: " << g.toString() << "\n";

    cout << "[main] -- block before ----\n";
    {
        auto p = rm::Person{"Cris P. Bacon", 27};
        cout << "[main] p: " << p.toString() << "\n";
        
        auto p2 = rm::Person{p};
        cout << "[main] p2: " << p2.toString() << "\n";

        auto p3 = rm::Person{};
        cout << "[main] p3: " << p3.toString() << "\n";
        
        p3 = p2;
        cout << "[main] p3: " << p3.toString() << "\n";

        cout << "[main] -- func ----\n";
        auto q = func(p);
        cout << "[main] q: " << q.toString() << "\n";
    }
    cout << "[main] -- block after ----\n";

    cout << "[main] -- vector block before --\n";
    {
        auto v = std::vector<rm::Person>{
            {"Anna",  27}, {"Berit", 37}, {"Carin", 47}
        };

        cout << "[main] -- print: element by copy ----\n";
        for (auto p : v) cout << "[copy] p: " << p.toString() << "\n";

        cout << "[main] -- print: element by const ref ----\n";
        for (auto const& p : v) cout << "[ref]  p: " << p.toString() << "\n";
        cout << "[main] -- just before end of block ----\n";
    }
    cout << "[main] -- vector block after --\n";
    cout << "[main] exit\n";
}
