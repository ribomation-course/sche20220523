#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <cstring>

namespace ribomation::domain {
    using std::cout;

    inline auto strClone(char const* str) -> char* {  //strdup() is using malloc()
        if (str == nullptr) return nullptr;
        return ::strcpy(new char[::strlen(str) + 1], str);
    }

    class Person {
        char* name = nullptr;
        unsigned age = 0;

    public:
        ~Person() {
            cout << "~Person @ " << this << "\n";
            delete[] name;
        }

        Person() {
            cout << "Person{} @ " << this << "\n";
            name = strClone("");
        }

        Person(char const* n, unsigned a) : name{strClone(n)}, age{a} {
            cout << "Person{const char* " << n << ", " << a << "} @ " << this << "\n";
        }

        Person(Person const&) = delete;
        Person& operator=(Person const&) = delete;

        Person(Person&& that) noexcept: name{that.name}, age{that.age} {
            that.name = nullptr;
            that.age  = 0;
            cout << "Person{Person&& " << &that << "} @ " << this << "\n";
        }

        Person& operator=(Person&& rhs) noexcept {
            if (this != &rhs) {
                delete[] name;
                name = rhs.name;
                rhs.name = nullptr;
                age = rhs.age;
                rhs.age = 0;
            }
            cout << "operator={Person&& " << &rhs << "} @ " << this << "\n";
            return *this;
        }

        auto incrAge() {
            return ++age;
        }

        [[nodiscard]] auto toString() const -> std::string {
            auto buf = std::ostringstream{};
            buf << "Person(" << (name ? name : "??") << ", " << age << ") @ " << this;
            return buf.str();
        }
    };

}

namespace rm = ribomation::domain;

auto func(rm::Person q) -> rm::Person {
    std::cout << "[func] q: " << q.toString() << "\n";
    q.incrAge();
    std::cout << "[func] q: " << q.toString() << "\n";
    return q;
}

auto g = rm::Person{"Anna Conda", 42};

int main() {
    using std::cout;

    cout << "[main] enter\n";
    cout << "[main] g: " << g.toString() << "\n";

    cout << "[main] -- block before ----\n";
    {
        auto p = rm::Person{"Cris P. Bacon", 27};
        cout << "[main] p: " << p.toString() << "\n";

        auto p2 = rm::Person{std::move(p)};
        cout << "[main] p2: " << p2.toString() << "\n";
        
        auto p3 = rm::Person{};
        cout << "[main] p3: " << p3.toString() << "\n";
        
        p3 = std::move(p2);
        cout << "[main] p3: " << p3.toString() << "\n";
        
        cout << "[main] -- func ----\n";
        auto q = func(std::move(p3));
        cout << "[main] q: " << q.toString() << "\n";
        cout << "[main] p: " << p.toString() << "\n";
        cout << "[main] p2: " << p2.toString() << "\n";
        cout << "[main] p3: " << p3.toString() << "\n";
    }
    cout << "[main] -- block after ----\n";

    cout << "[main] -- vector block before --\n";
    {
        auto v = std::vector<rm::Person>{};
        v.reserve(3'000);
        v.emplace_back("Anna", 27U);
        v.emplace_back("Berit", 37U);
        v.emplace_back("Carin", 47U);

//        cout << "[main] -- print: element by copy ----\n";
//        for (auto p : v) cout << "[copy] p: " << p.toString() << "\n";

        cout << "[main] -- print: element by const ref ----\n";
        for (auto const& p: v) cout << "[ref]  p: " << p.toString() << "\n";
        
        cout << "[main] -- just before end of block ----\n";
    }
    cout << "[main] -- vector block after --\n";
    cout << "[main] exit\n";
}
