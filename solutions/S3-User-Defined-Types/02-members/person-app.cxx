#include <iostream>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

using std::cout;
using std::string;
using namespace std::literals;

class Dog {
    const string name;
    static int   instance_count;
public:
    Dog() { ++instance_count; }
    Dog(string name) : name(std::move(name)) { ++instance_count; }
    Dog(Dog const& that) : name{that.name} { ++instance_count; }
    ~Dog() { --instance_count; }

    auto to_string() const {
        auto out = std::ostringstream{};
        out << "Dog{" << name << "} @ " << this;
        return out.str();
    }

    static int count() { return instance_count; }
};

int Dog::instance_count = 0;


class Person {
    string   name;
    unsigned age = 0;
    Dog* mydog = nullptr;
public:
    Person(string const& name_, unsigned age_)
            : name{name_}, age{age_} {
        cout << "Person{" << name << ", " << age << "} @ " << this << "\n";
    }
    ~Person() {
        cout << "~Person() @ " << this << "\n";
    }

    string to_string() const {
        auto out = std::ostringstream{};
        out << "Person{" << name
            << ", " << age
            << (mydog ? ", "s + mydog->to_string() : ""s)
            << "} @ " << this;
        return out.str();
    }

    auto getName() const -> string const& { return name; }

    auto getAge() const { return age; }

    void setName(string name_) { name = std::move(name_); }

    void setDog(Dog* dog_) { mydog = dog_; }
};


int main() {
    cout << "[main] enter\n";
//    {
//        auto p = Person{"Nisse Hult"s, 42};
//        cout << "p: " << p.to_string() << "\n";
//
//        auto fido = Dog{"Fido"s};
//        p.setDog(&fido);
//        cout << "p: " << p.to_string() << "\n";
//    }
    {
        cout << "[main] #dogs=" << Dog::count() << "\n";
        auto dogs = std::vector<Dog>{
                "Fido"s, "Caro"s, "Milou"s, "Lassie"s,
        };
        for (auto const& d: dogs) cout << d.to_string() << "\n";
        cout << "[main] #dogs=" << Dog::count() << "\n";
    }
    cout << "[main] exit #dogs=" << Dog::count() << "\n";
}
