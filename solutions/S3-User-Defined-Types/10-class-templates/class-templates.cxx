#include <iostream>
#include <deque>

namespace ribomation::utils {
    template<typename ElemType>
    class Queue {
        std::deque<ElemType> storage;
    public:
        void put(ElemType x) { storage.push_back(x); }
        ElemType get() {
            auto x = storage.front();
            storage.pop_front();
            return x;
        }
        auto empty() const { return storage.empty(); }
        auto size() const { return storage.size(); }
    };
}

namespace rm = ribomation::utils;

int main() {
    using std::cout;
    auto       q = rm::Queue<int>{};
    auto const N = 10;
    for (auto  k = 1; k <= N; ++k) q.put(k);
    cout << "size: " << q.size() << "\n";
    while (!q.empty()) cout << q.get() << " ";
}
