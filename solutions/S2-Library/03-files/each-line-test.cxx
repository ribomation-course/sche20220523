#include <iostream>
#include "each-line.hxx"

using std::cout;
using namespace ribomation::io;
using namespace std::string_literals;

int main() {
    each_line("../each-line-test.cxx"s, [](auto line) {
        cout << line << "\n";
    });
}
