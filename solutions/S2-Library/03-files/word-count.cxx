#include <iostream>
#include <sstream>
#include "each-line.hxx"

using std::string;
using std::cout;

struct Counts {
    const string filename;
    unsigned     lines = 0;
    unsigned     words = 0;
    unsigned     chars = 0;

    explicit Counts(string name) : filename{std::move(name)} {}

    void print() const {
        cout << filename << "\t" << lines << "\t" << words << "\t" << chars << "\n";
    }
};


int main(int argc, char** argv) {
    if (argc == 1) {
        auto      cnt = Counts{"STDIN"};
        ribomation::io::each_line(std::cin, [&cnt](auto line) {
            ++cnt.lines;
            cnt.chars += line.size() + 1;
            auto        buf = std::istringstream{line};
            for (string word; buf >> word;) cnt.words++;
        });
        cnt.print();
        return 0;
    }

    auto      total = Counts{"TOTAL"};
    for (auto k     = 1; k < argc; ++k) {
        auto filename = string{argv[k]};
        auto cnt      = Counts{filename};
        ribomation::io::each_line(filename, [&cnt](auto line) {
            ++cnt.lines;
            cnt.chars += line.size() + 1;
            auto        buf = std::istringstream{line};
            for (string word; buf >> word;) cnt.words++;
        });
        cnt.print();
        total.lines += cnt.lines;
        total.words += cnt.words;
        total.chars += cnt.chars;
    }
    total.print();
}
