#pragma once
#include <string>
#include <functional>
#include <fstream>

namespace ribomation::io {
    using std::string;
    using namespace std::string_literals;

    inline void each_line(std::istream& in, const std::function<void(string const&)>& consume) {
        for (string line; getline(in, line);) consume(line);
    }

    inline void each_line(string const& filename, const std::function<void(string const&)>& consume) {
        auto f = std::ifstream{filename};
        if (!f) throw std::invalid_argument{"cannot open "s + filename};
        each_line(f, consume);
    }

}

