#include <iostream>
#include <string>
#include <cctype>

using std::cout;
using std::string;
using namespace std::string_literals;

auto strip(string s)  {
    auto      res = ""s;
    for (auto k   = 0UL; k < s.size(); ++k) {
        auto ch = s[k];
        if (isalpha(ch)) res += ch;
    }
    return res;
}

int main() {
    auto s = "  abc .,+ ABC::123   ";
    cout << "'" << s << "' --> '" << strip(s) << "'\n";
}


