#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cmath>

using std::cout;
using std::begin;
using std::end;

int main() {
    auto in      = std::istream_iterator<double>{std::cin};
    auto eof     = std::istream_iterator<double>{};
    auto numbers = std::vector<double>{in, eof};

    auto [min, max] = std::minmax_element(begin(numbers), end(numbers));
    cout << "boundary: [" << *min << ", " << *max << "]" << "\n";

    auto N    = static_cast<double>(size(numbers));
    auto sum  = std::accumulate(begin(numbers), end(numbers), 0.0, [](auto sum, auto x) {
        return sum + x;  //strictly not needed, because it is the default op
    });
    auto mean = sum / N;
    cout << "mean: " << mean << "\n";

    auto sqsum  = std::accumulate(begin(numbers), end(numbers), 0.0, [mean](auto sum, auto x) {
        return sum + (x - mean) * (x - mean);
    });
    auto stddev = ::sqrt(sqsum / N);
    cout << "stddev: " << stddev << "\n";
}
