#include <iostream>
#include <string>
#include <random>
#include <algorithm>
#include <iterator>

int main(int argc, char** argv) {
    auto n     = argc == 1 ? 25U : std::stoi(argv[1]);
    auto r     = std::random_device{};
    auto gauss = std::normal_distribution<double>{10, 5};
    auto out   = std::ostream_iterator<double>{std::cout, "\n"};
    
    std::generate_n(out, n, [&gauss, &r] {
        return gauss(r);
    });
}
