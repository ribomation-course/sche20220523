#include <iostream>
#include <algorithm>

int main() {
    int        numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    auto const N         = sizeof(numbers) / sizeof(numbers[0]);

    std::for_each(numbers, numbers + N, [](auto x) {
        std::cout << x << " ";
    });
    std::cout << "\n";

     auto factor = 42;
     std::transform(numbers, numbers + N, numbers, [factor](auto x){
         return x*factor;
     });
    
    std::for_each(numbers, numbers + N, [](auto x) {
        std::cout << x << " ";
    });
}
