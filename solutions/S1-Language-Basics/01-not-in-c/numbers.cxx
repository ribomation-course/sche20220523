#include <iostream>
using std::cout;
using std::cin;

int main() {
    int count = 0, sum = 0, min = 9999, max = -9999;
    do {
        cout << "int (0 = exit)? ";
        int n;
        cin >> n;
        if (n == 0) break;
        ++count;
        sum += n;
        min = (n < min ? n : min);
        max = (n > max ? n : max);
    } while (true);
    if (count == 0) return 1;
    cout << "# values: " << count << "\n";
    cout << "average : " << (static_cast<double>(sum) / count) << "\n";
    cout << "min/max : " << min << "/" << max << "\n";
}
