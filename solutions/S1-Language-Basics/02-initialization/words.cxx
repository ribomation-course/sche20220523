#include <iostream>
#include <string>
#include <vector>
#include <set>
using namespace std::string_literals;
using std::cout;
using std::string;

int main() {
    {
        std::vector<string> words = {
                "cool"s, "is"s, "C++"
        };
        for (auto const& w: words) cout << w << " ";
    }
    cout << "\n";
    {
        std::set<string> words = {
                "cool"s, "is"s, "C++"
        };
        for (auto const& w: words) cout << w << " ";
    }
}
