#include <iostream>
using std::cout;

void fn(int val, int& ref) {
    ref *= 42;
    cout << "[fn] val=" << val << ", ref=" << ref << "\n";
}

int main() {
    int number = 10;
    cout << "before: number=" << number << "\n";
    fn(number, number);
    cout << "after: number=" << number << "\n";
}
