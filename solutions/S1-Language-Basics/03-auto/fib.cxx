#include <iostream>
#include <map>

using std::cout;

auto fib(unsigned n) {
    if (n == 0) return 0U;
    if (n == 1) return 1U;
    return fib(n - 2) + fib(n - 1);
}

struct Result {
    unsigned arg, res;
};

auto compute(unsigned n) {
    return Result{n, fib(n)};
}

auto table(unsigned n) {
    auto tbl = std::map<unsigned, unsigned>{};
    for (auto k = 1U; k <= n; ++k) {
        auto [a,r] = compute(k);
        tbl[a] = r;
    }
    return tbl;
}

int main() {
    auto const N = 10U;
    {
        cout << "fib(" << N << ") = " << fib(N) << "\n";
    }
    cout << "\n";
    {
        auto [a, r] = compute(N);
        cout << "fib(" << a << ") = " << r << "\n";
    }
    cout << "\n";
    {
        for (auto [a, r]: table(N))
            cout << "fib(" << a << ") = " << r << "\n";
    }
}
