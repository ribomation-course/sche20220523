# C++ Basics for C Developers
### 2022 May 23-25, 27

# Links
* [Installation Instructions](./installation-instructions.md)
* [Course Details](https://www.ribomation.se/programmerings-kurser/cxx/cxx-basics-for-c-programmers/)

# Course GIT Repo
It's recommended that you keep the git repo and your solutions separated.
Create a dedicated directory for this course and a sub-directory for
each chapter. Get the course repo initially by a `git clone` operation

![Git Clone](img/git-clone.png)

    mkdir -p ~/cxx-course/my-solutions
    cd ~/cxx-course
    git clone <git HTTPS clone link> gitlab

During the course, solutions will be push:ed to this repo and you can get these by
a `git pull` operation

    cd ~/cxx-course/gitlab
    git pull


# Build Solution/Demo Programs
The solutions and demo programs are all using CMake as the build tool. CMake is a cross-platform generator
tool that can generate makefiles and other build tool files. It is also the project descriptor for JetBrains
CLion, which is my IDE of choice for C/C++ development.

You don't have to use CLion in order to compile and run the sources.
What you do need to have; are `cmake`, `make` and `gcc/g++` all installed.
When you want to build a solution or demo:

First change into its project directory `cd path/to/some/solutions/dir`, then run the commands below
and the executable will be in the `./bld/` directory.

    cd some/project/folder/
    cmake -S . -B bld
    cmake --build bld


# Interesting Links
* [CppCon 2018: Jonathan Boccara “105 STL Algorithms in Less Than an Hour”](https://youtu.be/2olsGf6JIkU)
* [The World Map of C++ STL Algorithms](https://www.fluentcpp.com/getthemap/)
* [Online Tools Every C++ Developers Should Know - Thamara Andrade - CppCon 2021](https://youtu.be/UztsWf7F_Sc)
* [CppCon 2016: Jason Turner “Rich Code for Tiny Computers: A Simple Commodore 64 Game in C++17”](https://youtu.be/zBkNBP00wJE)
* [CppCon 2018: Michael Caisse “Modern C++ in Embedded Systems - The Saga Continues”](https://youtu.be/LfRLQ7IChtg)
* [Objects? No, thanks! (Using C++ effectively on small systems)](https://www.embedded.com/objects-no-thanks-using-c-effectively-on-small-systems/)
* [Real-Time-C++ Book, 4th ed](https://link.springer.com/book/10.1007/978-3-662-62996-3)
* [Real-Time-C++ Book GitHub repo](https://github.com/ckormanyos/real-time-cpp)
* [Compiler Explorer](https://godbolt.org/)
* [C++ Insights](https://cppinsights.io/)

# Additional
* [Addendum](./img/addendum.pdf)


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
